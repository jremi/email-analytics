var express = require('express');
var exphbs  = require('express-handlebars');
var request = require('request');
var bodyParser = require('body-parser');
var app = express();

var moment = require('moment');

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(express.static('public'));
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.get('/', function (req, res) {
    res.send('⚠️  <span style="font-family:courier;padding-left:8px"> Nothing here</span>')
});


//---------------- TESTING PIXEL TRACKING ---------------

app.get('/analytics/email/timespent/:pixel', function (req, res) {
    var pixel = req.params.pixel;
    var time = moment();
    var time_format = time.format('YYYY-MM-DD HH:mm:ss Z');
    var rootPixelPath = req.path;
    
    if(req.path.split('-@@-').length > 1){
        rootPixelPath = req.path.split('-@@-')[0];
    };

    if(pixel.split('-@@-').length > 1){
        pixel = pixel.split('-@@-')[0]; console.log('Email still open - ' + time_format);
    }
    else{
        console.log('Pixel Id: ' + pixel + ' - Open Email Time: ' + time_format);
    }
    setTimeout(function(){
        //console.log('Pixel Id: ' + pixel + ' ' + moment().format());
        res.redirect(307,'http://165.227.52.107:8080'+ rootPixelPath + '-@@-' + moment().format());
    },1000)
});

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

app.listen(port, function() {
    console.log('Our app is running on http://localhost:' + port);
});