$(document).ready(function(){
    var inviteUrl = $('#invite_code').val();    
    $("#share").jsSocials({
        url: inviteUrl,
        text: "Check out this book...",
        showLabel: true,
        showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
    });

    // Quick beacon test
    // window.addEventListener("unload", logData, false);
    
    // var open_email_time = moment.now();
    
    // function logData() {
    //     var close_email_time = moment.now();
    //     var total_time_elapsed = close_email_time - open_email_time;

    //     var analytics_data = {
    //         foo : '10:00:00',
    //         doo : '20:00:00'
    //     };
    //     navigator.sendBeacon("/log", JSON.stringify(analytics_data));
    // }

})
